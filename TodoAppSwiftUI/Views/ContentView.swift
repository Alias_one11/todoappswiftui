import SwiftUI

struct ContentView: View {
    // MARK: - ©State-->PROPERTIES
    @State private var showAddTodoView: Bool = false
    @State private var animateButton: Bool = false
    @State private var showSettingsView: Bool = false
    
    // MARK: - ©Environment-->PROPERTIES
    @Environment(\.managedObjectContext) var managedObjectContext
    
    // MARK: - ©FetchRequest-->PROPERTIES
    @FetchRequest(entity: Todo.entity(),
                  sortDescriptors: [
                    NSSortDescriptor(keyPath: \Todo.name, ascending: true)
                  ]) var todos: FetchedResults<Todo>
	
	/*©-----------------------------------------©*/

	// MARK: - Helper methods
	fileprivate func topTrailingbtnTapped() -> some View {
		return Button(action: { showSettingsView.toggle() },
					  label: { Image(systemName: "paintbrush") })
			.font(.title).font(Font.title.weight(.bold))
			// When the plus button is tapped, the app segues to AddTodoView()
			.sheet(isPresented: $showSettingsView) {
				SettingsView().preferredColorScheme(.dark)
			}
	}
	
	// Deletes todo
	fileprivate func deleteTodo(at items: IndexSet) {
		for index in items {
			
			let todo = todos[index]
			managedObjectContext.delete(todo)
			printf("Todo deleted: \(todo.name ?? "")")
			do {
				try managedObjectContext.save()
				
			} catch {
				printf("Could not delete todo..\n\(error.localizedDescription)")
			}
		}
	}
	
	// Add button bottom right of the view
	fileprivate func bottomTrailingBtnTapped() -> some View {
		return ZStack {
			
			Group {
				Circle().fill(Color.blue).opacity(animateButton ? 0.2 : 0)
					.scaleEffect(animateButton ? 1 : 0)
					.frame(width: 68, height: 68, alignment: .center)
				
				Circle().fill(Color.blue).opacity(animateButton ? 0.15 : 0)
					.scaleEffect(animateButton ? 1 : 0)
					.frame(width: 88, height: 88, alignment: .center)
			}// Gives the button continuos animation, forwards & backwards
			.animation(Animation.easeInOut(duration: 2).repeatForever(autoreverses: true))
			/*÷÷÷÷÷÷÷÷÷÷÷÷*/
			
			Button(action: {
				showAddTodoView.toggle()
			}, label: {
				Image(systemName: "plus.circle.fill")
					.resizable().scaledToFit()
					.background(Circle().fill(Color("ColorBase")))
					.frame(width: 48, height: 48, alignment: .center)
			})///:END-BUTTON
			.onAppear {
				animateButton.toggle()
			}
			/*÷÷÷÷÷÷÷÷÷÷÷÷*/
		}///:END ZSTACK
		.padding(.bottom, 15).padding(.trailing, 15)
	}
    
    /*©-----------------------------------------©*/
    var body: some View {
        
        NavigationView {
            
            ZStack {
                
                List {// UITableViewController
                    // Will list the todo items
                    ForEach(todos, id: \.self) { todo in
                        HStack {
                            Text(todo.name ?? " UNKNOWN")// On the left side
                            Spacer()
                            Text(todo.priority ?? " UNKNOWN")// On the right side
                        }
                        /**|     ⇪     |*/
                    }// END: [FOREACH]
                    
                    // Deletes todo at $0 index
                    .onDelete { indexSet in
                        deleteTodo(at: indexSet)
                    }
                    /*÷÷÷÷÷÷÷÷÷÷÷÷*/
                }/// END: [LIST]
                .navigationBarTitle("Todo", displayMode: .inline)
                // MARK: -#Plus+Button in navbar trailing
                // Adds a new todo & edits an existing todo
                .navigationBarItems(leading: EditButton(), trailing: topTrailingbtnTapped())
                
                // MARK: -#NO TODO ITEMS
                if todos.count == 0 {
                    EmptyListView()
                }
            }/// END: [ZSTACK]
            .sheet(isPresented: $showAddTodoView) {
                AddTodoView().preferredColorScheme(.dark)
                    // MARK: -#Core-Data setup
                    .environment(\.managedObjectContext, managedObjectContext)
            }///: .sheet
            // MARK: -#Add Button, bottom trailing(right-side)
            .overlay(VStack { bottomTrailingBtnTapped() } , alignment: .bottomTrailing)
            // END: [OVERLAY]
        }/// END: [NAVIGATION]
    }
    /*©-----------------------------------------©*/
}// END: [STRUCT]

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        // MARK: -#To get coredata to work in the preview
        let context = (UIApplication.shared.delegate as! AppDelegate)
            .persistentContainer.viewContext
        
        return ContentView().preferredColorScheme(.dark)
            .environment(\.managedObjectContext, context)
        //            .previewLayout(.fixed(width: 420, height: 820))
    }
}
