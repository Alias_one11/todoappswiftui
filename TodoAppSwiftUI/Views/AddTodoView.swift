import SwiftUI

struct AddTodoView: View {
    // MARK: - ©PROPERTIES
    let prioritiesList: [String] = [ "High 🔴", "Normal 🟢", "Low 🟡" ]
    
    // MARK: - ©State-->PROPERTIES
    @State private var name: String = ""
    @State private var priority: String = "Normal"
    @State private var errorShowing: Bool = false
    @State private var errorTitle: String = ""
    @State private var errorMsg: String = ""
    
    // MARK: - ©State-->ENVIRONMENT
    @Environment(\.presentationMode) var presentationMode
    @Environment(\.managedObjectContext) var managedObjectContext
    /*©--------------------------------------------------------------©*/
    
    var body: some View {
        
        NavigationView {
            /**|     ⇪     |*/
            VStack {
                
                VStack(alignment: .leading, spacing: 20) {
                    
                    // User input field-->TODO-NAME
                    TextField("Todo", text: $name).padding()
                        .background(Color(UIColor.tertiarySystemFill))
                        .cornerRadius(12)
                        .font(.system(size: 24, weight: .bold, design: .default))
                    
                    
                    // Picker-->TODO-PRIORITY
                    priorityPickerFunc(lblStr: "Priority", selection: $priority)
                    
                    // MARK: -#SAVE-Button
                    saveButtonTapped(name: name, priority: priority, txtStr: "Save")
                        .padding().frame(minWidth: 0, maxWidth: .infinity)
                        .background(Color.blue).cornerRadius(12)
                        .foregroundColor(.white)
                    
                }/// END: [VSTACK]
                .padding(.horizontal).padding(.vertical, 30)
                Spacer()
                
            }.navigationBarTitle("New Todo", displayMode: .inline)
            // MARK: -#Close-Button
            .navigationBarItems( trailing: closeAddTodoViewBtn() )
            // MARK: -#Error-->Alert-window
            .alert(isPresented: $errorShowing) {
                Alert(title: Text(errorTitle), message: Text(errorMsg),
                      dismissButton: .default(Text("OK"))
                )
            }
            
        }/// END: [NAVIGATIONVIEW]
    }// :body
    /*©-----------------------------------------©*/
    
    // MARK: - Helper methods
    typealias BindStr = Binding<String>
    internal func priorityPickerFunc(lblStr: String, selection: BindStr) -> some View {
        ///|     ⇪     |
        return Picker(lblStr, selection: selection) {
            ///|     ⇪     |
            ForEach(prioritiesList, id: \.self) {
                Text($0)
            }
        }.pickerStyle(SegmentedPickerStyle())
    }///:END
    
    func saveButtonTapped(name: String, priority: String, txtStr: String) -> Button<Text> {
        /**|     ⇪     |*/
        return Button(
            action: {
                if name != "" {
                    ///|     ⇪     |
                    let todo = Todo(context: managedObjectContext)
                    // Binding our @State properties with our core-data model properties
                    todo.name = name
                    todo.priority = priority
                    ///|     ⇪     |
                    // Saving our local data
                    do {
                        try managedObjectContext.save()
                        printf("NEW TODO: \(todo.name ?? ""), PRIORITY: \(todo.priority ?? "")")
                    } catch {
                        printf("Could not save new todo.\n\(error.localizedDescription)")
                    }
                } else {
                    errorShowing = true
                    errorTitle = "Invalid Entry.."
                    errorMsg = "Enter a valid todo entry.."
                }
                // Dismisses the view once the todo is added
                presentationMode.wrappedValue.dismiss()
                /**|     ⇪     |*/
            }, label: {
                Text(txtStr)
                    .font(.system(size: 24, weight: .bold, design: .default))
            })
    }///:END
    
    fileprivate func closeAddTodoViewBtn() -> some View {
        return Button(action: { presentationMode.wrappedValue.dismiss() },
                      label: {
                        configuredSFIcon(cRadius: 5, fillColor: .init("ColorYoutube"),
                                         sfSymbol: "xmark", fgColor: .white,
                                         frameWidth: 26, frameHeight: 26)
                      })
        
    }///:END
    /*©-----------------------------------------©*/
}// END: [STRUCT]

struct AddTodoView_Previews: PreviewProvider {
    static var previews: some View {
        AddTodoView().preferredColorScheme(.dark)
            .previewLayout(.fixed(width: 420, height: 820))
    }
}
