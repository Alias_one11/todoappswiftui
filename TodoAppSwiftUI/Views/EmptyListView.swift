import SwiftUI

struct EmptyListView: View {
    // MARK: - ©PROPERTIES
    let imageList: [String] = [
        "illustration-no1",
        "illustration-no2",
        "illustration-no3",
    ]
    
    let tipsList: [String] = [
        "Use your time wisely.",
        "Slow and steady wins the race.",
        "Keep it short and sweet.",
        "Put hard tasks first.",
        "Reward yourself after work.",
        "Collect tasks ahead of time.",
        "Each night schedule for tomorrow."
      ]
    
    // MARK: - ©State-->PROPERTIES
    @State private var isAnimated: Bool = false
    
    /*©-----------------------------------------©*/
    var body: some View {
        
        ZStack {
            VStack(alignment: .center, spacing: 20) {
                
                // BACKGROUND IMAGE randomizes when the view is reloaded
                configuredImgFunc(imgStr: "\(imageList.randomElement() ?? imageList[0])")
                
                // HEADLINE randomizes when the view is reloaded
                Text("\(tipsList.randomElement() ?? tipsList[0])").layoutPriority(0.5)
                    .font(.system(.headline, design: .rounded))
            }
            .padding(.horizontal)
            // Adding animation
            .opacity(isAnimated ? 1 : 0)
            .offset( y: isAnimated ? 0 : -50).animation(.easeOut(duration: 1.5))
            .onAppear {
                isAnimated.toggle()
            }
            
        }///:END-ZSTACK
        .frame(minWidth: 0, maxWidth: .infinity,
               minHeight: 0, maxHeight: .infinity)
        .background(Color("ColorBase"))
        .edgesIgnoringSafeArea(.all)
    }
    /*©-----------------------------------------©*/
    
    // MARK: - Helper methods
    fileprivate func configuredImgFunc(imgStr: String) -> some View {
        return Image(imgStr).resizable().scaledToFit()
            .frame(minWidth: 256, idealWidth: 280, maxWidth: 360,
                   minHeight: 256, idealHeight: 280, maxHeight: 360,
                   alignment: .center).layoutPriority(1)
    }
    
}// END: [STRUCT]

struct EmptyListView_Previews: PreviewProvider {
    static var previews: some View {
        EmptyListView().preferredColorScheme(.dark)
            .previewLayout(.fixed(width: 420, height: 820))
    }
}
