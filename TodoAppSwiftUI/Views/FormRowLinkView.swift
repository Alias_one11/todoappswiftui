import SwiftUI

struct FormRowLinkView: View {
	// MARK: - ©PROPERTIES
	var color: Color
	var icon, linkTitle, link: String
	
	/*©-----------------------------------------©*/
	var body: some View {
		HStack {
			///|__________|
			// GLOBE_ICON
			configuredSFIcon(cRadius: 8, fillColor: color, sfSymbol: icon, fgColor: .white,
							 imgScale: .large, frameWidth: 36, frameHeight: 36, alignment: .center)
			
			// BUTTON
			configuredSFSymbolWithLink(sfSymbol: "chevron.right", linkTitle: linkTitle, urlLinkStr: link)
			
		}///:END__>HSTACK
	}
	/*©-----------------------------------------©*/
}// END: [STRUCT]

struct FormRowLinkView_Previews: PreviewProvider {
	///|__________|
	static var previews: some View {
		
		FormRowLinkView(color: .pink, icon: "globe", linkTitle: "Website",
						link: "https://gitlab.com/Alias_one11/todoappswiftui")
			///|__________|
			.preferredColorScheme(.dark)
			.previewLayout(.fixed(width: 375, height: 60)).padding()
	}
}
