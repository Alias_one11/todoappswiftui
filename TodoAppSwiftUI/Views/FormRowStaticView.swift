import SwiftUI

struct FormRowStaticView: View {
    // MARK: - ©PROPERTIES
    var icon, firstTxt, secondTxt: String
    
    /*©-----------------------------------------©*/

    
    var body: some View {
        ///|     ⇪     |
        HStack {
            // GEAR_ICON
            configuredSFIcon(cRadius: 8, fillColor: Color.gray, sfSymbol: icon,
                               fgColor: .white, frameWidth: 36, frameHeight: 36)
            
            // FIRST_TEXT
            Text(firstTxt).foregroundColor(.gray).padding(.leading, 10)
            Spacer()// Horizontally placed leading
            // SECOND_TEXT
            Text(secondTxt).padding(.trailing, 10)
            
        }///:END__>HSTACK
    }
    /*©-----------------------------------------©*/
    
}// END: [STRUCT]

struct FormRowStaticView_Previews: PreviewProvider {
    
    static var previews: some View {
        ///|__________|
        FormRowStaticView(icon: "gear", firstTxt: "Application", secondTxt: "Todo")
            .preferredColorScheme(.dark)
            .previewLayout(.fixed(width: 375, height: 60))
    }
}
