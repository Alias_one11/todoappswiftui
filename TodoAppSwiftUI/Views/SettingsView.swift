import SwiftUI

struct SettingsView: View {
    // MARK: - ©PROPERTIES
    let footerStr = "Copyright © All rights reserved.\nBetter Apps Less Code"
    
    // MARK: -@Environment-PROPERTIES
    @Environment(\.presentationMode) var presentationMode
    
	/*©-----------------------------------------©*/
	// MARK: - Helper methods
	fileprivate func section3() -> some View {
		return // MARK: - SECTION #3
			Section(header: Text("Follow us on social media")) {
				// Website link
				FormRowLinkView(color: .pink, icon: "globe", text: "Website",
								link: "https://gitlab.com/Alias_one11/todoappswiftui")
				// Twitter link
				FormRowLinkView(color: .blue, icon: "link", text: "Twitter",
								link: "https://twitter.com/Alias_One11")
				
				FormRowLinkView(color: .init("ColorYoutube"), icon: "play.rectangle", text: "Youtube",
								link: "https://www.youtube.com/channel/" +
									"UCn8u_HliNg32-p4KjDJTddg?view_as=subscriber")
				
			}///:END__>SECTION 3
			.padding(.vertical, 3)
	}
	
	fileprivate func section4() -> some View {
		
		Section(header: Text("About the application")) {
			///|__________|
			///|Application|
			FormRowStaticView(icon: "gear", firstTxt: "Application", secondTxt: "Todo")
			///|Complatibilty|
			FormRowStaticView(icon: "checkmark.seal", firstTxt: "Compatibilty", secondTxt: "iPhone, iPad")
			///|Developer|
			FormRowStaticView(icon: "keyboard", firstTxt: "Developer", secondTxt: "Alias / Anna")
			///|Designer|
			FormRowStaticView(icon: "paintbrush", firstTxt: "Designer", secondTxt: "Wachi-Man")
			///|Version|
			FormRowStaticView(icon: "flag", firstTxt: "Version", secondTxt: "1.0.2")
		}///:END__>SECTION 4
		.padding(.vertical, 3)
	}
	
    /*©-----------------------------------------©*/
    var body: some View {
        
        NavigationView {
            
            VStack(alignment: .center, spacing: 0) {
                
                Form {
                    // MARK: - SECTION #3
                    section3()
                    // MARK: - SECTION #4
                    section4()
                    
                    
                }///:END__>FORM
                .listStyle(GroupedListStyle())
                .environment(\.horizontalSizeClass, .regular)
                /*÷÷÷÷÷÷÷÷÷÷÷÷*/
                
                // MARK: -#FOOTER
                // will drop the text all the way to the bottom of the form
               footerText(footer: footerStr)
                
            }///:END__>VSTACK
            // Creating a button in the nav-bar trailing to dismiss the view
            .navigationBarItems(trailing:
                                    Button(action: { presentationMode.wrappedValue.dismiss() },
                                           label: {
                                            configuredSFIcon(cRadius: 5, fillColor: .init("ColorYoutube"),
                                                             sfSymbol: "xmark", fgColor: .white,
                                                             frameWidth: 26, frameHeight: 26)
                                           })///:END__>Button
            )///:END__>.navigationBarItems
            .navigationBarTitle("Settings", displayMode: .inline)
            .background(Color("ColorBackground")).edgesIgnoringSafeArea(.all)
            /*÷÷÷÷÷÷÷÷÷÷÷÷*/
        }///:END__>NAVIGATIONVIEW
    }
    /*©-----------------------------------------©*/
}//:END__>STRUCT

struct SettingsView_Previews: PreviewProvider {
    static var previews: some View {
        SettingsView().preferredColorScheme(.dark)
    }
}


